---
title: "The Legacy of Project Apollo"
date: 2021-03-29T12:49:27+00:00
featureImage: images/allpost/allPost-1.jpeg
postImage: images/single-blog/AS17-134-20381.jpg
---

I have been meditating on Neil Armstrong's spirit, Project Apollo, and his legacy in the long lens of history. At the start of Apollo, no one ever expected it to be this impactful. They knew that they were making history, but they simultaneously overestimated the short-term enthusiasm for their project and underestimated its long-term impact. Most people at the time misunderstood the program and thought that it would be just one note in a longer symphony. But then we didn't go back to the moon. We prioritized the F-35s of the world before achievements like Apollo, and that note became the one and only note most people had ever heard.

The astronauts themselves didn't attempt to be this famous or be this defined by this achievement. This misunderstanding became much clearer when the program ended and all of the Apollo astronauts faced an existential crisis and a deep depression following their crowning achievement. Their lives - the lives of some of the smartest and most driven people to ever live - peaked in their 40's and that was it.

It's hard to imagine a different perception of Apollo than the one we have right now, but again hindsight is different than foresight. The agency didn't seem to grasp the magnitude of their achievement - look at how they treated the original video tapes that had been transformed for live TV video. It was a thing, it happened and that was that. The rockets, science etc were more valuable and no one was focused on the mission itself from our current hagiographic perspective.

Their oversight came with a cost. No one - not even NASA - realized the obvious at the time; the astronauts were never going to be human beings after their moon walks. They were going to be such and such who walked on the moon. And in a way - in the eyes of history - every act following that point was essentially a footnote. After all, how the heck do you top walking on the moon?

This burden was further magnified for Armstrong. Most of us forget the depth of his fame. Carl Sagan wrote about an anecdote where an anthropologist told him that a previously uncontacted tribe (or rather assumed to be uncontacted tribe) asked about Apollo 11 and if it was true if human beings had indeed walked on the moon. Try to put yourself in his shoes and lift the weight he carried. Try to imagine being Neil Armstrong and waking up every single day with the weight that every literate child in the world will learn your name until humanity itself ceases to be. He became The First Man - not a person who was allowed to make mistakes. No, that was too undignified for The First Man.

And he hated every second of it. He refused to sign autographs. He stopped going out into public. Stopped giving interviews. ([the last interview he ever gave was to an accountant](https://www.theatlantic.com/national/archive/2012/05/neil-armstrong-grants-rare-interview-accountant/327730/)) And chose to live his life as a recluse.

But perhaps, this tendency is why he was chosen in the first place. Deke Slayton and the other administrators, wisely, knew that he would be a better First Man than someone like Aldrin, who can be described in the most charitable of terms as a [fame seeker.](http://www.americaspace.com/?p=24709)

And so one of the most shy and cerebral men of his generation was chosen to be a "living monument." And perhaps a monument to the American era as a whole. As of writing, Armstrong is already more famous than Alexander the Great - after whom at least three major languages have defined the word "great." Even now his name isn't Alexander of Macedonia, but Alexander *the* Great.

Alexander himself will be forgotten before Armstrong is. He is without a doubt, the most famous human to ever exist. And long after the American empire ceases to be, he will still be remembered as an example of what we achieved. Barring a calamity, he will be remembered for all time as long as human beings are alive.

It was a very heavy burden, but Armstrong bore it with grace. Perhaps with greater grace than Washington himself, who exemplified the ideal of doing your service and saying goodbye to retire to a farm.

Beyond his technical mind. I am in awe of him as a human being. The more I learn about him, the more I admire him.
